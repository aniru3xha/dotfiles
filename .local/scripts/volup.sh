#!/bin/bash

wpctl set-volume @DEFAULT_SINK@ 5%+

vol=$(wpctl get-volume @DEFAULT_SINK@ | cut -c 11-)
notify-send -i /usr/share/icons/AdwaitaLegacy/32x32/mimetypes/audio-x-generic.png -h int:value:"$vol" "Volume"
