#! /bin/bash

rclone mount onedrive: --vfs-cache-mode full ~/.local/storage/onedrive &
rclone mount gdrive: --vfs-cache-mode full ~/.local/storage/gdrive &
