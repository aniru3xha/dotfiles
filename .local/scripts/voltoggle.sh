#!/bin/bash

wpctl set-mute @DEFAULT_SINK@ toggle

vol=$(wpctl get-volume @DEFAULT_SINK@ | cut -c 14-)
if [[ -n "$vol" ]]; then
  notify-send -t 400 -i /usr/share/icons/AdwaitaLegacy/32x32/mimetypes/audio-x-generic.png "mute"
else
  notify-send -t 400 -i /usr/share/icons/AdwaitaLegacy/32x32/mimetypes/audio-x-generic.png "unmute"
fi
